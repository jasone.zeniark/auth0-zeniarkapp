// @ts-ignore
// ignore import from from '@env', will fix soon -Jason
import { AUTH0_CLIENT_ID, AUTH0_DOMAIN } from '@env';
import jwtDecode from 'jwt-decode';
import React, { useEffect, useState } from 'react';
import { Alert, Platform } from 'react-native';
import Auth0 from 'react-native-auth0';
import SInfo from 'react-native-sensitive-info';

const auth0 = new Auth0({
  domain: AUTH0_DOMAIN,
  clientId: AUTH0_CLIENT_ID,
});

const AuthContext = React.createContext<any>(undefined);

const AuthContextProvider = (props: any) => {
  const [loading] = useState(true);
  const [loggedIn, setLoggedIn] = useState<boolean | null>(null);
  const [userData, setUserData] = useState<any>(null);

  const getUserData = async (idKey?: string) => {
    if (Platform.OS === 'ios') {
      const accessToken = idKey
        ? idKey
        : await SInfo.getItem('accessToken', {});

      const data = await auth0.auth.userInfo({ token: accessToken });
      return data;
    }
    if (Platform.OS === 'android') {
      const idToken = idKey ? idKey : await SInfo.getItem('idToken', {});
      const { name, picture, exp } = jwtDecode<any>(idToken);

      if (exp < Date.now() / 1000) {
        throw new Error('ID token expired!');
      }

      return {
        name,
        picture,
      };
    }
  };

  // executed on first app load
  useEffect(() => {
    (async () => {
      if (Platform.OS === 'ios') {
        try {
          const data = await getUserData();

          setLoggedIn(true);
          setUserData(data);
        } catch (err) {
          console.log('err: ', err);

          try {
            const refreshToken = await SInfo.getItem('refreshToken', {});
            const newAccessTokenResponse = await auth0.auth.refreshToken({
              refreshToken,
            });

            await SInfo.setItem(
              'accessToken',
              newAccessTokenResponse.accessToken,
              {}
            );

            const _userData = getUserData(newAccessTokenResponse.accessToken);

            setUserData(_userData);
            setLoggedIn(true);
          } catch (err) {
            console.log('error with refreshing token..');
            setLoggedIn(false);
          }
        }
      }
      if (Platform.OS === 'android') {
        try {
          if (loggedIn) {
            const user_data = await getUserData();
            if (user_data) {
              setLoggedIn(true);
              setUserData(user_data);
            }
          }
        } catch (err) {
          Alert.alert('Error logging in');
        }
      }
    })();
  }, [loggedIn]);

  // executed when user just logged in
  useEffect(() => {
    (async () => {
      try {
        if (loggedIn) {
          const data = await getUserData();
          setUserData(data);
        }
      } catch (err) {
        console.log('error logging in: ', err);
      }
    })();
  }, [loggedIn]);

  const login = async () => {
    if (Platform.OS === 'ios') {
      try {
        const credentials = await auth0.webAuth.authorize({
          scope: 'openid offline_access profile email',
        });

        await SInfo.setItem('accessToken', credentials.accessToken, {});
        await SInfo.setItem('refreshToken', credentials.refreshToken ?? '', {});

        setLoggedIn(true);
      } catch (err) {
        console.log('error logging in..', err);
      }
    }
    if (Platform.OS === 'android') {
      try {
        const credentials = await auth0.webAuth.authorize({
          scope: 'openid email profile',
        });
        await SInfo.setItem('idToken', credentials.idToken ?? '', {});
        const user_data = await getUserData(credentials.idToken);
        setLoggedIn(true);
        setUserData(user_data);
      } catch (err) {
        Alert.alert('Error logging in');
      }
    }
  };

  const logout = async () => {
    if (Platform.OS === 'ios') {
      try {
        await auth0.webAuth.clearSession({});

        await SInfo.deleteItem('accessToken', {});
        await SInfo.deleteItem('refreshToken', {});

        setLoggedIn(false);
        setUserData(null);
      } catch (err) {
        console.log('error logging out..', err);
      }
    }
    if (Platform.OS === 'android') {
      try {
        await auth0.webAuth.clearSession({});
        await SInfo.deleteItem('idToken', {});
        setLoggedIn(false);
        setUserData(null);
      } catch (err) {
        Alert.alert('Error logging in');
      }
    }
  };

  const value = {
    loading,
    loggedIn,
    login,
    logout,
    userData,
  };

  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
};

export { AuthContext, AuthContextProvider };
