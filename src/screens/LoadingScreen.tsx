import { StackActions } from '@react-navigation/native';
import React, { useContext, useEffect } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { AuthContext } from '../context/AuthContext';

const LoadingScreen = ({ navigation }: any) => {
  const { loading, loggedIn } = useContext<any>(AuthContext);

  useEffect(() => {
    if (loggedIn) {
      navigation.dispatch(StackActions.replace('Account'));
    }

    if (!loggedIn) {
      navigation.dispatch(StackActions.replace('Login'));
    }
  }, [loggedIn, navigation]);

  return (
    <View style={styles.container}>
      {loading && (
        <React.Fragment>
          <ActivityIndicator size="large" />
          <View style={{ marginTop: 10 }}>
            <Text>Please wait...</Text>
          </View>
        </React.Fragment>
      )}
    </View>
  );
};

export default LoadingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
