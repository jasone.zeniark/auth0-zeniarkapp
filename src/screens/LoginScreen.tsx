import { StackActions } from '@react-navigation/native';
import React, { useContext, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, withTheme } from 'react-native-paper';
import { AuthContext } from '../context/AuthContext';

const LoginScreen = ({ navigation, theme }: any) => {
  const { colors } = theme;

  const { loggedIn } = useContext<any>(AuthContext);

  useEffect(() => {
    if (loggedIn) {
      navigation.dispatch(StackActions.replace('Account'));
    }
  }, [loggedIn, navigation]);

  const { login } = useContext<any>(AuthContext);

  return (
    <View style={[styles.container, { backgroundColor: colors.background }]}>
      <Button mode="contained" onPress={() => login()}>
        Login with Auth0
      </Button>
    </View>
  );
};

export default withTheme(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 30,
    paddingLeft: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
